from pymongo import MongoClient
import json

connSingleton = None

conf = {}
with open("pymongo_config.json", "r") as f:
    conf = json.load(f)

def GetConf():
    return conf

def GetConn(password):
    global connSingleton
    if connSingleton is None:
        
        connSingleton = MongoClient('mongodb://admin:'+password+'@'+conf['ip']+':'+str(conf['port'])+'/admin?authSource=admin')
    return connSingleton