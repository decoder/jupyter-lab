from decoderpkm import GetConf, GetConn
from IPython.display import HTML, display
from IPython.display import HTML, display
from bson.json_util import dumps
import pymongo
from pymongo import TEXT
import json
from json2html import *

import pymongo
import json

conn = GetConn()
conf = GetConf()

db = conn[conf['db']]

sourceCodeCollection = db.Sourcecode
traceCollection = db.TraceabilityMatrix
sourceCodeCollection.create_index(
    [('fileName', TEXT)], default_language='english')


def GetAllRequirements():

    # Search for all requirements
    reqs = sourceCodeCollection.find({"$text": {
        "$search": "requirements requirement TR FR \"technical requirement\" \"functional requirement\" \"non functional requirement\" nfr"}})

    # Display all the requirements as an HTML table
    for req in reqs:
        requirement_in_html = ''
        requirement_in_html = json2html.convert(json=req)

        display(HTML(requirement_in_html))
        #print(json.dumps(req, indent=4, sort_keys=True))


def GetRequirementsNumberOfGlobalTypes():

    reqs = sourceCodeCollection.find({"$text": {
        "$search": "requirements requirement TR FR \"technical requirement\" \"functional requirement\" \"non functional requirement\" nfr"}})
    dictTable = {}
    uniqueGlobals = []

    # Get a dictionary with the number of global types for each project
    for req in reqs:
        dictTable[req['fileName']] = {}
        gTypes = req['globals']

        for gType in gTypes:
            if list(gType.keys())[0] in dictTable[req['fileName']].keys():
                dictTable[req['fileName']][list(gType.keys())[0]] += 1
            else:
                dictTable[req['fileName']][list(gType.keys())[0]] = 1
            if not list(gType.keys())[0] in uniqueGlobals:
                uniqueGlobals.append(list(gType.keys())[0])

    # Create HTML code for the table
    tableHtml = "<table><tr><th>Path</th>"
    for u in uniqueGlobals:
        tableHtml += "<th>"+u+"</th>"
    tableHtml += "</tr>"

    for key, value in dictTable.items():
        tableHtml += "<tr><td>"+key+"</td>"
        for u in uniqueGlobals:
            tableHtml += "<td>"+str(value.get(u))+"</td>"
        tableHtml += "</tr>"

    # Draw actual table with IPython
    display(HTML(tableHtml))


def GetRequirementByName(name=str):
    requirement_in_html = ''
    my_search_format = "\"{}\""
    my_search = my_search_format.format(name)

    # Search for all requirements
    reqs = sourceCodeCollection.find({"$text": {
        "$search": "requirements requirement TR FR \"technical requirement\" \"functional requirement\" \"non functional requirement\" nfr"}})

    for req in reqs:
        # Search for the requirement that matches that name
        reqByName = sourceCodeCollection.find_one(
            {"_id": req._id}, {"$text": {"$search": my_search}})

        # If that requirement is found is displayed as an HTML table
        if reqByName is not None:

            requirement_in_html = json2html.convert(json=reqByName)

            display(HTML(requirement_in_html))
