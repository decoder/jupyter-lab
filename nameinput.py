import ipywidgets as widgets
from IPython.display import display


class name_input():
    def __init__(self,
                 name="e.g. 'Decoder'"
                 ):
        self.name = widgets.Text(value=name,
                                 placeholder='Paste epic`s name here!',
                                 description='Name:')

        self.name.on_submit(self.handle_submit)
        display(self.name)

    def handle_submit(self, text):
        self.v = text.value
        return self.v
