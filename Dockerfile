# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

FROM jupyter/scipy-notebook:python-3.8.8

RUN python3 -m pip install --no-cache jupyterhub==1.4.0

# Install jupyterlab
RUN python3 -m pip install jupyterlab
RUN jupyter serverextension enable --py jupyterlab --sys-prefix

#Install Python packages needed
RUN pip install jupyter_contrib_nbextensions
RUN python3 -m pip install json2html

# Install python libraries
RUN python3 -m pip install pymongo

# Copy Discern project folder
# RUN mkdir /home/jovyan/tbcnn/
# COPY --chown=jovyan:users ./tbcnn /home/jovyan/tbcnn
COPY --chown=jovyan:users ./ /home/jovyan/examples
# Grant permissions to folder
RUN chmod -R 777 /home/jovyan/examples
COPY ./jupyter_notebook_config.py /etc/jupyter/jupyter_notebook_config.py

# Install Data Dons Discern dependencies
# RUN pip install -r /home/jovyan/examples/tbcnn/requirements.txt
# RUN conda env update --name myenv1 --file /home/jovyan/tbcnn/dependencies.yml

RUN python3 -m pip install GitPython==3.1.17
RUN python3 -m pip install Werkzeug==1.0.1
RUN python3 -m pip install zipfile38==0.0.3
RUN python3 -m pip install scipy==1.6.2
RUN python3 -m pip install pytest==6.2.3
RUN python3 -m pip install pandas==1.2.4
RUN python3 -m pip install matplotlib==3.3.4
RUN python3 -m pip install numpy==1.20.0
RUN python3 -m pip install gensim==4.0.1
RUN python3 -m pip install torch==1.8.1

# Adding Grant access to user jovyan
USER root

RUN \
    sed -i /etc/sudoers -re 's/^%sudo.*/%sudo ALL=(ALL:ALL) NOPASSWD: ALL/g' && \
    sed -i /etc/sudoers -re 's/^root.*/root ALL=(ALL:ALL) NOPASSWD: ALL/g' && \
    sed -i /etc/sudoers -re 's/^#includedir.*/## **Removed the include directive** ##"/g' && \
    echo "jovyan ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    chmod g+w /etc/passwd

USER jovyan
