import ipywidgets as widgets
from pymongo import MongoClient
from decoderpkm import GetConn
import pymongo

def show_project_widget(password):
    client = GetConn(password)

    db = client['pkm']

    projects = db.Projects

    projectNames = []

    for project in projects.find({}):
        projectNames.append(project['name'])

    projectW = widgets.Dropdown(
        options=projectNames,
        value='mythaistar',
        description='Project:',
        disabled=False,
    )

    return projectW